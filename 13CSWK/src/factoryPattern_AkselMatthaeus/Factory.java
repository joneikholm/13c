package factoryPattern_AkselMatthaeus;

public class Factory {

	public Product produce(String type){

		switch (type){

		case "one": return new ProductOne();

		case "two": return new ProductTwo();

		case "three": return new ProductThree();

		}

		return null;

	}

}
