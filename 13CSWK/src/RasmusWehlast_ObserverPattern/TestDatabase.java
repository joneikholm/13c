package RasmusWehlast_ObserverPattern;

public class TestDatabase {
	public static void main(String[] args) {
		
		Database db = new Database();
		
		db.registerObserver(new Observer("Archiver"));
		db.registerObserver(new Observer("Editor"));
		db.registerObserver(new Observer("Client"));
		
		db.notifyObserver();
		
		db.changeRecord(2);
		
		db.notifyObserver();
		
		db.editOperation("create");
		
		db.notifyObserver();
		
	}
}
