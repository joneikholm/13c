package RasmusWehlast_ObserverPattern;

public interface Subject {
	
	public void registerObserver(Observer o);
	
	public void notifyObserver();
	
	public void editOperation(String operation);
	
	public void changeRecord(int record);
	
}
