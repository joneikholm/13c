package RasmusWehlast_ObserverPattern;

public class Observer implements ObserverI{
	
	private String name;
	
	public Observer(String name) {
		this.name = name;
	}
	
	@Override
	public String notifyO() {
		return name;
	}

}
