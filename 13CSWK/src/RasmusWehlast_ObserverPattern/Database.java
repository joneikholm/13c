package RasmusWehlast_ObserverPattern;
import java.util.Vector;


public class Database implements Subject {
	
	private Vector<Observer> observers = new Vector<Observer>();
	private String operation;
	private int record;
	
	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
		record = 1;
		operation = "delete";
	}

	@Override
	public void notifyObserver() {
		for (Observer o : observers) {
			System.out.println(o.notifyO() + ": a " + operation + " was performed on record " + record);
		}
	}

	@Override
	public void editOperation(String operation) {
		this.operation = operation;
		
	}

	@Override
	public void changeRecord(int r) {
		this.record = r;
		
	}
	
	
}
