
import static org.junit.Assert.*;

import org.junit.Test;
import org.mockito.Mockito;

public class Test1 {

	@Test
	public void test() {
		//we want to test the StudentsController but without
		//testing the StudentsRepository class that it depends on
		//at runtime.
		
		//Arrange
		//If we do the following two lines of code (1), we create a unit test that tests the controller AND the StudentsRepository = BAD!
		//We wan't to test the smallest possible unit in this example only the controller. Not the StudentsRepository (that saves to a database).
		//Never use a database in a unit test. It might not contain the data you expect.
		
		//(1)
		//IStudentsRepository repository = new StudentsRepository(); //notice the difference.
		//StudentsController controller = 	new StudentsController(repository);
		
		//notice the difference here 
		StudentsRepository repository = Mockito.mock(StudentsRepository.class); //now we have a mocked version.
		//IStudentsRepository repository = Mockito.mock(StudentsRepository.class); //now we have a mocked version.
		StudentsController controller = 	new StudentsController(repository);
		Student s = new Student(0, "Per Larsen", "per@larsen.dk");
		
		//Act
		controller.saveStudent(s);
		controller.saveStudent(s);
		
		//Assert
		//We assert that the repository's saveStudent method was called once
		//with the parameter s
		//If it was called the saveStudent method of the controller works and we
		//tested the controller without using the Repository that saves to a database.
		Mockito.verify(repository, Mockito.times(2)).saveStudent(s);
		
	}
}
