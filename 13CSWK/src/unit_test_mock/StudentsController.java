
public class StudentsController {
	private IStudentsRepository studentsRepository;
	
	//constructor dependency injection.
	public StudentsController(IStudentsRepository sRep)
	{
		studentsRepository = sRep;
	}
	
	
	//The Gui will call this method with a Student obj. that may or may not be valid
	public void saveStudent(Student s)
	{
		if (s.validate())
		{
			studentsRepository.saveStudent(s);
		}
	}
}
