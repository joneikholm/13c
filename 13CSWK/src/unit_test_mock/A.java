
public class A {
	//1
	//private B b = new B();
	
	//2: S�� kan vi unit teste
	//klassen A uden at teste B!
	private B b;
	public A(B b)
	{
		this.b = b;
	}
	
	public void run(int a)
	{
		if (a > 0)
		{
			b.doSomething();
		}
	}
}
