
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.*;


public class TestLoginController {

	DBManager dbManagerMock1;
	DBManager dbManagerMock2;
	
	@Before
	public void setup(){
		dbManagerMock1=mock(DBManager.class);
		dbManagerMock2=mock(DBManager.class);
	}
	
	@Test
	public void test() {
		
		DBManager dbManagerMock = mock(DBManager.class);
		
		LoginController loginController = new LoginController(dbManagerMock);
		
		loginController.checkBruger("hanna");  // lige som f������r
		
//		// Opretter et ArgumentCaptor object, som er i stand til at lytte p������ given metode i et givent objekt
		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		
//		// s������t ArgumentCaptor objektet til at registrere argumentet til metoden checkUserStatus i objektet dbManagerMock
		verify(dbManagerMock).checkUserStatus(captor.capture());
		
	
		// unders������ger om de to strenge er ens: den forventede og den som blev modtaget som argument i metodekaldet
		assertEquals("hanna", captor.getValue());
		
		loginController.checkBruger("hanna2");  // lige som f������r
		
//		 unders������ger hvor mange gange metoden checkUserStatus er blevet kaldt. 
		verify(dbManagerMock, times(1)).checkUserStatus("hanna2");
	}
	
	@Test
	public void testTypes(){
		Integer in = new Integer(12);
		assertThat( in, instanceOf(Integer.class));
	}
	
	@Test
	public void testOrder(){  //her tester vi r������kkef������lge af metodekald. 
		dbManagerMock1.checkUserStatus("a");
		dbManagerMock2.checkUserStatus("b");
		InOrder inOrder = inOrder(dbManagerMock1, dbManagerMock2);
		inOrder.verify(dbManagerMock1).checkUserStatus("a");
		inOrder.verify(dbManagerMock2).checkUserStatus("b");
		
	}

}














