package decorator_RasmusD;

public class DecoratorTest {

	public static void main(String[] args) {

		Song song = new Song();
		song = new Synthesizer(song);
		song = new Percussion(song);
		song = new Vocals(song);
		song = new Drums(song);
		
		System.out.println("You're listening to a " + song.description() + ".");

	}
}
