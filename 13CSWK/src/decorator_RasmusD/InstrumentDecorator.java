package decorator_RasmusD;

public abstract class InstrumentDecorator extends Song {

	public abstract String description();
}
