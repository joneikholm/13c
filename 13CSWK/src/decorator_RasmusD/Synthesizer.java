package decorator_RasmusD;

public class Synthesizer extends InstrumentDecorator {

	Song song;
	
	public Synthesizer(Song s) {
		
		song = s;
	}
	
	public String description() {
		
		return song.description() + " with a synthesizer";
	}

}
