package decorator_RasmusD;

public class Vocals extends InstrumentDecorator {

	Song song;
	
	public Vocals(Song s) {
		song = s;
	}
	
	public String description() {
		return song.description() + " with vocals";
	}

}
