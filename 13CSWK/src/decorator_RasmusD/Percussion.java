package decorator_RasmusD;

public class Percussion extends InstrumentDecorator {

	Song song;
	
	public Percussion(Song s) {
		
		song = s;
	}
	
	public String description() {
		return song.description() + " with percussion";
	}
}
