package decorator_RasmusD;

public class Drums extends InstrumentDecorator {

	Song song;
	
	public Drums(Song s) {
		song = s;
	}
	
	public String description() {
		return song.description() + " with drums";
	}

}
