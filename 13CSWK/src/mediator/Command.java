package mediator;

//Colleague interface
interface Command {
  void execute();
}