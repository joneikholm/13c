package mediator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;



class MediatorDemo extends JFrame implements ActionListener {

   IMediator mediator = new Mediator();

   MediatorDemo() {
       JPanel p = new JPanel();
       p.add(new BtnView(this, mediator));
       p.add(new BtnBook(this, mediator));
       p.add(new BtnSearch(this, mediator));
       getContentPane().add(new LblDisplay(mediator), "North");
       getContentPane().add(p, "South");
       setSize(400, 200);
       setVisible(true);
   }

   public void actionPerformed(ActionEvent ae) {
       Command comd = (Command) ae.getSource();
       comd.execute();
   }

   public static void main(String[] args) {
       new MediatorDemo();
   }

}
