package unit_test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
//Example, used to demonstrate test first (TDD)
//Write the tests first

public class CarTest {

	Car car;
	
	@Before
	public void setUp()
	{
		//Arrange
		car = new Car();
	}
	
	@Test
	public void setHeight_SmallValidNumber_Equals() throws Exception {
		
		//Act
		car.setHeight(0.6);
		
		//Assert.
		assertEquals(0.6, car.getHeight(), 0.01);
	}

	@Test
	public void setHeight_LargeValidNumber_Equals() throws Exception
	{
		car.setHeight(1000.6);
		assertEquals(1000.6, car.getHeight(), 0.1);
	}
	
	
	@Test(expected=Exception.class) //Assert
	public void setHeight_InvalidNumberEdgeCase_ThrowsException() throws Exception
	{
		car.setHeight(0.5);
	}
	
	@Test(expected=Exception.class) //Assert
	public void setHeight_InvalidNumberVerySmall_ThrowsException() throws Exception
	{
		car.setHeight(-1000);
	}	
}
