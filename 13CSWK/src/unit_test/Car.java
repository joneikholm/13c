package unit_test;
//Example, used to demonstrate test first (TDD)
//Write the tests first

public class Car {
	private double height;
	private int numberOfWheels;
	private double length;
	
	
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) throws Exception {
		if (height > 0.5)
		{
			this.height = height;
		}
		else
		{
			throw new Exception("Invalid input");
		}
	}
	
	
	public int getNumberOfWheels() {
		return numberOfWheels;
	}
	public void setNumberOfWheels(int numberOfWheels) {
		
	}
	
	
	
	public double getLength() {
		return length;
	}
	
	public void setLength(double length) {
		
	}
}
