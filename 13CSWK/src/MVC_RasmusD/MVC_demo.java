package MVC_RasmusD;

public class MVC_demo {

	public static void main(String[] args) {
		Model model = getCarsFromDatabase();
		View view = new View();
		Controller controller = new Controller(view, model);
		controller.updateView();
	}
	
	private static Model getCarsFromDatabase() {
		Model model = new Model();
		model.setBrand("Audi");
		model.setColor("Black");
		return model;
	}
}