package MVC_RasmusD;

public class Controller {
	private View view;
	private Model model;

	public Controller(View view, Model model) {
		this.view = view;
		this.model = model;
	}
	
	public void setColor(String color) {
		model.setColor(color);
	}
	
	public String getColor() {
		return model.getColor();
	}
	
	public void setBrand(String brand) {
		model.setBrand(brand);
	}
	
	public String getBrand() {
		return model.getBrand();
	}
	
	public void updateView() {
		view.printCars(model.getColor(), model.getBrand());
	}
}

