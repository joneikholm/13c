package flyweight_RasmusWE;

public class EmployeeDB {
	
	private String[] names = {"Rasmus", "John", "Lars"};
	
	private String[] pos = {"Direkt�r", "slave", "revisor"};
	
	private int[] age = {21, 42, 80};
	
	private int size = 3; 
	
	private Employee e;
	private int index = 0;
	
	public EmployeeDB() {
		e = new Employee(names[index], pos[index], age[index]);
		index++;
	}
	
	public void getEmployees() {
		
		for (int i = 0; i < size; i++) {
			
			System.out.println("Navn: " + e.getName() + "\nAlder: " + e.getAge() + "\nStilling: " + e.getPosition());
			System.out.println();
			
			e.setName(names[index]);
			e.setAge(age[index]);
			e.setPosition(pos[index]);
		
			if (index != size - 1)
			index++;
			
		}
		
		
	}
	
}
