package flyweight_RasmusWE;

public class Employee {

	public String name, position;
	
	public int age;
	
	public Employee(String name, String pos, int age) {
		this.name = name;
		position = pos;
		this.age = age;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
